﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WayPoints : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject waypoints;
    Transform[] points;
    public int destpoint = 0;
    // Start is called before the first frame update
    void Start()
    {
        points = waypoints.GetComponentsInChildren<Transform>();
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        GoToNextPoint();
    }
    void GoToNextPoint()
    {
        if (points.Length == 0)
        {
            return;
        }
        agent.SetDestination(points[destpoint].position);

        destpoint = (destpoint + 1) % points.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance < 0.05f)
        {
            GoToNextPoint();
        }
        


    }
}
