﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//copyright brackeys 
//edited by Dylan Thom
public class terrainGereration : MonoBehaviour
{
    public int width = 500; //input valuse for the hight of the terrain 
    public int height = 500; //input valuse for the width of the terrain 
    public int depth = 20; //input valuse for the depth of the terrain 
    public float scale = 20f; //input valuse forchanging the terrains scaling 
    public float offSetX = 100f;
    public float offSetY = 100f;

    // Start is called before the first frame update
    void Start()
    {
        //this creats the random terrain hight and with of the mountons.
        offSetX = Random.Range(0f, 9999f);
        offSetY = Random.Range(0f, 9999f);
      
    }
     void Update()
    {
        //this gets the terrains data and components needed.
         Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }
    TerrainData GenerateTerrain(TerrainData terrainData)
    {
        //this is the section that changes the terrains data values.
        terrainData.heightmapResolution = width + 1; 
        terrainData.size = new Vector3(width, depth, height);
        terrainData.SetHeights(0, 0, GenerateHights());
        return terrainData;
    }
    float[,] GenerateHights() //this is the maths section for the terrain generation.
    {
        float[,] heights = new float[width, height];
        for(int x = 0; x < width; x++)
        {
            for(int y = 0;y < height; y++)
            {
                heights[x, y] = CalculateHeight(x, y);
            }
        }
        return heights;
    }
    float CalculateHeight(int x,int y)
    {
        float xCoord = (float)x / width * scale + offSetX;
        float yCoord = (float)y / height * scale + offSetY;

        return Mathf.PerlinNoise(xCoord, yCoord);
    }


}
