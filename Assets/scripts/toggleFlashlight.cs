﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleFlashlight : MonoBehaviour
{
    public Light theLight;
    public bool on = true;

    // Start is called before the first frame update
    void Start()
    {
        on = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (on == true)
            {
                theLight.enabled = false;
                on = false;
            }
            else if (on == false)
            {
                theLight.enabled = true ;
                on = true;
            }
           
        }
    }
}
