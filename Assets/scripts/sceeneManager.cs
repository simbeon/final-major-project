﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceeneManager : MonoBehaviour
{
    public Animator animator;

    private int levelToLoad;

    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        FadeToNextLevel();
    }
    public void FadeToNextLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void FadeToLevel(int LevelIndex)
    {
        levelToLoad = LevelIndex;
        animator.SetTrigger("fadeout");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }



}
